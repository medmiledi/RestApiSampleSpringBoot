package com.milamed.demo.services;

import com.milamed.demo.entities.User;

import java.util.List;

public interface UserService {
    User saveOrUpdate(User user);
    User getUser(Long id);
    List<User> getAll();
    void delete(User user);
}
