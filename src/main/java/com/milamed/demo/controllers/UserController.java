package com.milamed.demo.controllers;

import com.milamed.demo.entities.User;
import com.milamed.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/saveOrUpdate")
    public User saveOrUpdate(@RequestBody User user){
        return userService.saveOrUpdate(user);
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable Long id){
        return userService.getUser(id);
    }

    @GetMapping("/all")
    public List<User> getAll(){
        return userService.getAll();
    }

    @DeleteMapping("/delete")
    public void delete(@RequestBody User user){
        userService.delete(user);
    }
}
