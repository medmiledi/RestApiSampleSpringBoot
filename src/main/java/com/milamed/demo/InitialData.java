package com.milamed.demo;

import com.milamed.demo.entities.User;
import com.milamed.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InitialData implements ApplicationRunner {
    @Autowired
    private UserRepository userRepository;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        createUserIfNotExist();
    }
    private void createUserIfNotExist(){
        List<User> userList ;
        userList = userRepository.findAll();
        if(userList.isEmpty()){
            User user = new User();
            user.setFirstName("Jhon");
            user.setLastName("Doe");
            user.setMail("jhon@doe.com");
            user.setPassword("123456");
            userRepository.saveAndFlush(user);
        }
    }
}
